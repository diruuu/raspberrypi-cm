from gpiozero import Button
import subprocess
import os
import time
from signal import pause
from websocket import startWebsocket
from get_env import getEnv
from libcamera import openLibcamera
from print_paper import printPaper
from buzzer import buzzerBeepPhoto, buzzerBeepError, buzzerBeepPrinting
from image_upload import upload_to_aws
import shutil

button = Button(14)
buttonPrint = Button(18)

fehProcessPID = None

filepath = "/home/pi/__temp__/"
targetpath = "/home/pi/photos/"
filename = "_temp_photo.jpg"

def openPhoto():
  global fehProcessPID
  buzzerBeepPhoto()
  fehProcess = subprocess.Popen(["feh", "-qrYF", "fill", filepath + filename], env=getEnv())
  fehProcessPID = fehProcess.pid
  print("Feh PID: ", fehProcessPID)

def onPrint():
  buzzerBeepPrinting()
  photoNum = str(int(time.time()))[2:10]
  shutil.copy(filepath + filename, targetpath + photoNum + ".jpg")
  isUploaded = upload_to_aws(photoNum)
  # Notify if image failed to upload
  if (not isUploaded):
    buzzerBeepError()
  printPaper(photoNum, isUploaded)
  # Close preview
  if fehProcessPID is not None:
    closingPreview()

pid = openLibcamera(filename = filename, on_capture = openPhoto)

print(pid)

def capturing():
  global fehProcessPID
  if fehProcessPID is None:
    print("Capturing")
    # Send signal to capture photo
    subprocess.run(["kill", "-SIGUSR1", str(pid)])
  else:
    buzzerBeepError()

def closingPreview():
  global fehProcessPID
  if fehProcessPID is not None:
    print("Closing preview")
    subprocess.run(["kill", "-SIGTERM", str(fehProcessPID)])
    fehProcessPID = None
  else:
    buzzerBeepError()

def onButtonPress():
  global fehProcessPID
  if fehProcessPID is None:
    capturing()
  else:
    closingPreview()

def onConnect():
  buzzerBeepPhoto()

def onDisconnect():
  buzzerBeepError()

button.when_pressed = onButtonPress
buttonPrint.when_pressed = onPrint

startWebsocket(onConnect = onConnect, onDisconnect = onDisconnect, onShutter = capturing, onLongPress = onPrint)