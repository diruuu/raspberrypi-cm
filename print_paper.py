from escpos import *
from buzzer import buzzerBeepPhoto, buzzerBeepError

def printPaper(photoNum, isUploaded):
    p = printer.Usb(0x0fe6, 0x811e, timeout=0, in_ep=0x81, out_ep=0x01)
    if (p.is_online()):
      buzzerBeepPhoto()
      p.set(align='center', bold=True)
      p.text("KUBIK PHOTOBOOTH\n")
      p.set(align='center', bold=False),
      p.text("Jl. Sungai Kambang, No 12.\n")
      p.text("Jambi. WA. 081373098196\n")
      p.text("------------------------------\n\n")
      p.qr(content="https://wa.me/14155238886?text=join%20recently-pine", size=6, native=False, center=True)
      p.text("No foto: " + photoNum + "\n")
      p.set(align='left')
      p.text("\nScan QR Code untuk mendownload \nfoto melalui Whatsapp. ")
      p.text('Lalu \nkirim pesan berisi "')
      p.set(align='left', bold=True, underline=1)
      p.text(photoNum)
      p.set(align='left', bold=False, underline=0)
      p.text('"')

      p.cut()
      p.close()
    else:
      buzzerBeepError()