from subprocess import Popen, PIPE, STDOUT
from threading import Thread
import os
from get_env import getEnv


def openLibcamera(filename, on_capture = None):
    my_env = os.environ.copy()
    my_env["DISPLAY"] = ":0"

    child = Popen([
      "/home/pi/libcamera-apps/build/libcamera-still", 
      "-o", "__temp__/" + filename,
      "--hflip",
      "--gain", "10",
      "-t", "0",
      "--width", "1200",
      "--height", "800",
      "-s",
      "-f",
      "--post-process-file", "/home/pi/libcamera-apps/assets/annotate_cv.json",
      "--viewfinder-width", "1200",
      "--viewfinder-height", "800",
      # "--mode", "2592:1944:10:U",
      "--viewfinder-mode", "1600:1400:10:U",
      "--flush",
      ], env=getEnv(), stdin=PIPE, stdout=PIPE, stderr=STDOUT)

    def deal_with_stdout():
      with open("stderr.txt","w") as err:
        for line in child.stdout:
          err.write(line.decode("utf-8"))
          if ("Still capture image received" in line.decode("utf-8")):
            if on_capture:
              on_capture()

    t = Thread(target=deal_with_stdout, daemon=True)
    t.start()

    return child.pid