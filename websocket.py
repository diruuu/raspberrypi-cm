import asyncio
import websockets

def handlerMaker(onConnect = None, onDisconnect = None, onShutter = None, onLongPress = None):
  async def handler(websocket, path):
      while True:
          try:
              message = await websocket.recv()
          except websockets.ConnectionClosedError:
              if (onDisconnect):
                onDisconnect()
              break
          except websockets.ConnectionClosedOK:
              if (onDisconnect):
                onDisconnect()
              break
          if message == "SEND_SHUTTER_EVENT":
            if (onShutter):
              onShutter()
            else:
              print(message)
          elif message == "SEND_LONG_PRESS_EVENT":
            if (onLongPress):
              onLongPress()
            else:
              print(message)
          elif message == "CONNECTED":
            if (onConnect):
              onConnect()
            else:
              print(message)
          elif message == "DISCONNECT":
            if (onDisconnect):
              onDisconnect()
            else:
              print(message)
          else:
            print(message)
  return handler

async def main(onConnect = None, onDisconnect = None, onShutter = None, onLongPress = None):
    print("Starting websocket at http://0.0.0.0:8000")
    async with websockets.serve(handlerMaker(onConnect = onConnect, onDisconnect = onDisconnect, onShutter = onShutter, onLongPress = onLongPress), "0.0.0.0", 8000):
        await asyncio.Future()

def startWebsocket(onConnect = None, onDisconnect = None, onShutter = None, onLongPress = None):
    asyncio.run(main(onConnect = onConnect, onDisconnect = onDisconnect, onShutter = onShutter, onLongPress = onLongPress))