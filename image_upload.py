import boto3
from botocore.exceptions import NoCredentialsError

def upload_to_aws(filename):
    s3 = boto3.client('s3')
    try:
        file_location = '/home/pi/photos/' + filename +'.jpg'
        print("Uploading to...", file_location)
        s3.upload_file(file_location, 'kubik-photos', filename + '.jpg', ExtraArgs={'ContentType': "image/jpeg"})
        print("Upload Successful")
        return True
    except FileNotFoundError:
        print("The file was not found")
        return False
    except NoCredentialsError:
        print("Credentials not available")
        return False 