import os

def getEnv():
    my_env = os.environ.copy()
    my_env["DISPLAY"] = ":0"
    return my_env