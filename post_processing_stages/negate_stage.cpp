/* SPDX-License-Identifier: BSD-2-Clause */
/*
 * Copyright (C) 2021, Raspberry Pi (Trading) Limited
 *
 * negate_stage.cpp - image negate effect
 */

#include <libcamera/stream.h>

#include "core/frame_info.hpp"
#include "core/libcamera_app.hpp"

#include "post_processing_stages/post_processing_stage.hpp"

// #include <Magick++.h>

using Stream = libcamera::Stream;

class NegateStage : public PostProcessingStage
{
public:
	NegateStage(LibcameraApp *app) : PostProcessingStage(app) {}

	char const *Name() const override;

	void Read(boost::property_tree::ptree const &params) override {}

	void Configure() override;

	bool Process(CompletedRequestPtr &completed_request) override;

private:
	Stream *stream_;
	StreamInfo info_;
};

#define NAME "negate"

char const *NegateStage::Name() const
{
	return NAME;
}

void NegateStage::Configure()
{
	stream_ = app_->GetMainStream();
	info_ = app_->GetStreamInfo(stream_);
}

bool NegateStage::Process(CompletedRequestPtr &completed_request)
{
	// std::vector<libcamera::Span<uint8_t>> const &buffers = app_->Mmap(completed_request->buffers[stream_]);
	// libcamera::Span<uint8_t> buffer = buffers[0];

	FrameInfo info(completed_request->metadata);
	info.sequence = completed_request->sequence;

	// unsigned char *ptr = (unsigned char *)buffer.data();

	// Magick::Blob my_blob(buffer.data(), buffer.size());
	// Magick::Image first(my_blob);

	// Magick::Image second("/home/pi/overlay1.png");
	// first.composite(second, 150, 150);

	return false;
}

static PostProcessingStage *Create(LibcameraApp *app)
{
	return new NegateStage(app);
}

static RegisterStage reg(NAME, &Create);
