/* SPDX-License-Identifier: BSD-2-Clause */
/*
 * Copyright (C) 2021, Raspberry Pi (Trading) Limited
 *
 * annotate_cv_stage.cpp - add text annotation to image
 */

// The text string can include the % directives supported by FrameInfo.

#include <libcamera/stream.h>

#include "core/frame_info.hpp"
#include "core/libcamera_app.hpp"

#include "post_processing_stages/post_processing_stage.hpp"

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"

using namespace cv;

using Stream = libcamera::Stream;

class AnnotateCvStage : public PostProcessingStage
{
public:
	AnnotateCvStage(LibcameraApp *app) : PostProcessingStage(app) {}

	char const *Name() const override;

	void Read(boost::property_tree::ptree const &params) override;

	void Configure() override;

	bool Process(CompletedRequestPtr &completed_request) override;

private:
	Stream *stream_;
	StreamInfo info_;
	Mat png_image;
};

#define NAME "annotate_cv"

char const *AnnotateCvStage::Name() const
{
	return NAME;
}

void AnnotateCvStage::Read(boost::property_tree::ptree const &params)
{
	png_image = imread("/home/pi/cat.jpeg", IMREAD_COLOR);
}

void AnnotateCvStage::Configure()
{
	stream_ = app_->GetMainStream();
	if (!stream_ || stream_->configuration().pixelFormat != libcamera::formats::YUV420)
		throw std::runtime_error("AnnotateCvStage: only YUV420 format supported");
	info_ = app_->GetStreamInfo(stream_);
}

bool AnnotateCvStage::Process(CompletedRequestPtr &completed_request)
{
	libcamera::Span<uint8_t> buffer = app_->Mmap(completed_request->buffers[stream_])[0];
	FrameInfo info(completed_request->metadata);
	info.sequence = completed_request->sequence;

	uint8_t *ptr = (uint8_t *)buffer.data();

	Mat mYUV(info_.height * 1.5, info_.width, CV_8UC1, ptr, info_.stride);
	Mat mRGB(info_.height, info_.width, CV_8UC3);
	cvtColor(mYUV, mRGB, COLOR_YUV2BGR_I420, 3);

	png_image.copyTo(mRGB(Rect(100, 100, png_image.cols, png_image.rows)));

	cvtColor(mRGB, mYUV, COLOR_BGR2YUV_I420, 1);

	return false;
}

static PostProcessingStage *Create(LibcameraApp *app)
{
	return new AnnotateCvStage(app);
}

static RegisterStage reg(NAME, &Create);
