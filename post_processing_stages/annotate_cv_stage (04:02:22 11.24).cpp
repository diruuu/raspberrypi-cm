/* SPDX-License-Identifier: BSD-2-Clause */
/*
 * Copyright (C) 2021, Raspberry Pi (Trading) Limited
 *
 * annotate_cv_stage.cpp - add text annotation to image
 */

// The text string can include the % directives supported by FrameInfo.

#include <libcamera/stream.h>

#include "core/frame_info.hpp"
#include "core/libcamera_app.hpp"

#include "post_processing_stages/post_processing_stage.hpp"

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"

using namespace cv;

using Stream = libcamera::Stream;

class AnnotateCvStage : public PostProcessingStage
{
public:
	AnnotateCvStage(LibcameraApp *app) : PostProcessingStage(app) {}

	char const *Name() const override;

	void Read(boost::property_tree::ptree const &params) override;

	void Configure() override;

	bool Process(CompletedRequestPtr &completed_request) override;

private:
	Stream *stream_;
	StreamInfo info_;
	std::string text_;
	int fg_;
	int bg_;
	double scale_;
	int thickness_;
	double alpha_;
	double adjusted_scale_;
	int adjusted_thickness_;
	Mat png_image;
	Mat yuv;
	Mat png_image_converted;
};

#define NAME "annotate_cv"

char const *AnnotateCvStage::Name() const
{
	return NAME;
}

void AnnotateCvStage::Read(boost::property_tree::ptree const &params)
{
	text_ = params.get<std::string>("text");
	fg_ = params.get<int>("fg", 255);
	bg_ = params.get<int>("bg", 0);
	scale_ = params.get<double>("scale", 1.0);
	thickness_ = params.get<int>("thickness", 2);
	alpha_ = params.get<double>("alpha", 0.5);
	png_image = imread("/home/pi/bingung.png", IMREAD_UNCHANGED);
}

void AnnotateCvStage::Configure()
{
	stream_ = app_->GetMainStream();
	if (!stream_ || stream_->configuration().pixelFormat != libcamera::formats::YUV420)
		throw std::runtime_error("AnnotateCvStage: only YUV420 format supported");
	info_ = app_->GetStreamInfo(stream_);
}

bool AnnotateCvStage::Process(CompletedRequestPtr &completed_request)
{
	libcamera::Span<uint8_t> buffer = app_->Mmap(completed_request->buffers[stream_])[0];
	FrameInfo info(completed_request->metadata);
	info.sequence = completed_request->sequence;

	uint8_t *ptr = (uint8_t *)buffer.data();

	Mat im(info_.height, info_.width, CV_8U, ptr, info_.stride);
	Mat rgb_im;
	// Convert YUV to RGBA
	Mat imClone = im.clone();
	cvtColor(imClone, rgb_im, COLOR_YUV2RGB_I420, 0);
	// Copy PNG to camera stream
	png_image.copyTo(rgb_im(Rect(0, 0, png_image.cols, png_image.rows)));
	// Convert back to YUV
	cvtColor(rgb_im, im, COLOR_RGB2YUV_I420, 0);
	return false;
}

static PostProcessingStage *Create(LibcameraApp *app)
{
	return new AnnotateCvStage(app);
}

static RegisterStage reg(NAME, &Create);
