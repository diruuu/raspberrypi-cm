from gpiozero import TonalBuzzer
from gpiozero.tones import Tone
import time

buzzer = TonalBuzzer(17)

def buzzerBeepPhoto():
  buzzer.play(buzzer.max_tone) 
  time.sleep(0.1)
  buzzer.stop()
  time.sleep(0.05)
  buzzer.play(buzzer.min_tone)
  time.sleep(0.3)
  buzzer.stop()

def buzzerBeepError():
  buzzer.play(buzzer.max_tone) 
  time.sleep(0.2)
  buzzer.stop()
  time.sleep(0.1)
  buzzer.play(buzzer.max_tone) 
  time.sleep(0.2)
  buzzer.stop()
  time.sleep(0.1)
  buzzer.play(buzzer.max_tone) 
  time.sleep(0.2)
  buzzer.stop()
  time.sleep(0.1)

def buzzerBeepPrinting():
  buzzer.play(buzzer.mid_tone) 
  time.sleep(0.3)
  buzzer.stop()
  time.sleep(0.05)
  buzzer.play(buzzer.min_tone) 
  time.sleep(0.1)
  buzzer.stop()
  time.sleep(0.05)
  buzzer.play(buzzer.max_tone) 
  time.sleep(0.2)
  buzzer.stop()
